# AnalysisPowerPulsing
**Maintainer**: Estel Perez Codina  
**Module Type**: *DUT*  
**Module Type**: *Timepix3*  
**Status**: Immature

### Description
Analysis of power pulsing behavior of Timepix3 chips.

### Usage
```toml
[AnalysisPowerPulsing]

```
