# TestAlgorithm
**Maintainer**: Simon Spannagel (<simon.spannagel@cern.ch>), Daniel Hynds (<daniel.hynds@cern.ch>)  
**Module Type**: *DETECTOR*  
**Detector Type**: *all*  
**Status**: Functional   

### Description
This module collects `pixel` and `cluster` objects from the clipboard and creates correlation and timing plots with respect to the reference detector.


### Parameters
* `makeCorrelatons`: Boolean to change if correlation plots should be outputted. Default value is `false`.
* `doTimingCut`: Boolean to switch on/off the cut on cluster times for correlations. Defaults to `false`.
* `timingCut`: maximum time difference between clusters to be taken into account. Only used if `doTimingCut` is set to `true`, defaults to `100ns`.

### Plots produced
For each device the following plots are produced:

* 2D hitmap
* 2D event times histogram
* Correlation in X
* Correlation in Y
* 2D correlation in X in global coordinates
* 2D correlation in Y in global coordinates
* 2D correlation in X in local coordinates
* 2D correlation in Y in local coordinates
* Correlation times (nanosecond binning) histogram, range covers 2 * event length
* Correlation times (integer values) histogram

### Usage
```toml
[TestAlgorithm]
makeCorrelations = true
```
